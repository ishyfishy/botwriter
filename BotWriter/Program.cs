﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using XCommas.Net.Objects;

namespace BotWriter
{
	public class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Welcome to BotWriter (Telegram @ismail_oz)");
			Console.WriteLine();

			Console.WriteLine("Please write your API key:");
			var inputApiKey = Console.ReadLine();

			Console.WriteLine("Please write your API secret:");
			var inputApiSecret = Console.ReadLine();

			Console.WriteLine();
			Console.WriteLine();
			
			var xCommas = new XCommas.Net.XCommasApi(inputApiKey, inputApiSecret);
			var botList = new List<Bot>();

			botList.AddRange(xCommas.GetBots(limit: 200, botScope: BotScope.Enabled).Data);
			botList.AddRange(xCommas.GetBots(limit: 200, botScope: BotScope.Disabled).Data);

			botName:

			Console.WriteLine($"Welcome to BotWriter for 3Commas. Please choose one of your bots below (total {botList.Count}):");
			Console.WriteLine();

			foreach (var bot in botList)
			{
				Console.Write(bot.Name);
				Console.Write(" | ");
			}

			Console.WriteLine();
			Console.WriteLine();
			
			var inputBotName = Console.ReadLine();

			var selectedBot = botList.FirstOrDefault(x => string.Equals(x.Name, inputBotName, StringComparison.InvariantCultureIgnoreCase));
			if(selectedBot == null)
			{
				Console.WriteLine($"Bot '{inputBotName}' doesn't exist. Try again please.");
				goto botName;
			}

			Console.WriteLine();
			Console.WriteLine();

			Console.WriteLine($"You have selected {selectedBot.Name}:");
			Console.WriteLine(JsonConvert.SerializeObject(selectedBot));

			Console.WriteLine("Do you want to overwrite all other bots with this setting? Write 'ok' if you want to.");
			
			var inputApprovement = Console.ReadLine();

			if(!string.Equals(inputApprovement, "ok", StringComparison.InvariantCultureIgnoreCase))
			{
				goto botName;
			}

			foreach(var bot in botList)
			{
				// Strategy
				bot.Strategy = selectedBot.Strategy;
				bot.BaseOrderVolume = selectedBot.BaseOrderVolume;
				bot.SafetyOrderVolume = selectedBot.SafetyOrderVolume;
				bot.LeverageType = selectedBot.LeverageType;
				bot.Type = selectedBot.Type;
				bot.BaseOrderVolumeType = selectedBot.BaseOrderVolumeType;
				bot.SafetyOrderVolumeType = selectedBot.SafetyOrderVolumeType;
				bot.ProfitCurrency = selectedBot.ProfitCurrency;

				// Take profit
				bot.TakeProfit = selectedBot.TakeProfit;
				bot.TakeProfitType = selectedBot.TakeProfitType;
				bot.TrailingEnabled = selectedBot.TrailingEnabled;
				bot.TrailingDeviation = selectedBot.TrailingDeviation;

				// Signals
				bot.Strategies = selectedBot.Strategies;

				// Safety orders
				bot.MaxSafetyOrders = selectedBot.MaxSafetyOrders;
				bot.MaxActiveDeals = selectedBot.MaxActiveDeals;
				bot.SafetyOrderStepPercentage = selectedBot.SafetyOrderStepPercentage;
				bot.MartingaleVolumeCoefficient = selectedBot.MartingaleVolumeCoefficient;
				bot.MartingaleStepCoefficient = selectedBot.MartingaleStepCoefficient;

				// Advanced settings
				bot.Cooldown = selectedBot.Cooldown;
				bot.MinVolumeBtc24h = selectedBot.MinVolumeBtc24h;
				bot.MinPrice = selectedBot.MinPrice;
				bot.MaxPrice = selectedBot.MaxPrice;

				xCommas.UpdateBot(bot.Id, new BotUpdateData(bot));

				Console.WriteLine();
				Console.WriteLine(JsonConvert.SerializeObject(bot));
				Console.WriteLine();
			}

			Console.WriteLine();
			Console.WriteLine($"Updated {botList.Count} bots.");
		}
	}
}